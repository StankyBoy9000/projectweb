(function (angular) {
    var app = angular.module("app");

    app.controller("inboxCtrl", ["$http", "$state", function ($http, $state) {

        var that = this;

        this.message = {
            "message_from" : null,
            "username": "",
            "content": "",
            "dateCreated": null,
            "message_to" : null
        }
        this.allMessages = [];



        this.sendMessage = function(){ 
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            var hh = today.getHours();
            var min = today.getMinutes();

            if (dd < 10) {
            dd = '0' + dd;
            }

            if (mm < 10) {
            mm = '0' + mm;
            }

            today = yyyy + '/' + mm + '/' + dd + " " + hh + ":" + min;
            that.message.dateCreated = today;
            $http.post("/inbox/sendMessage", that.message).then(function(response){
                console.log("Poslata poruka! Kod: " + response.status);
                that.getMessages();
                console.log(that.allMessages)
            }, function(response) {
                console.log(response.status) 
            });
        };

        
        this.getMessages = function(){
            $http.get("/inbox/messages").then(function(response){
                if(response.data.length > 0){
                    that.allMessages = response.data;
                    console.log(response.data)
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
            });
        };

        this.getMessages();

    }]);
})(angular);