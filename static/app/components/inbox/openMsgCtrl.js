(function (angular) {
    var app = angular.module("app");


    app.controller("openMsgCtrl", ["$stateParams", "$http", "$state", function($stateParams, $http, $state) {

        var that = this;

        this.messageThread = [];
        this.messageSender = null;
        this.notSender = null;
        this.messageReciever = null;
        this.getMessageThread = function(id) {
            $http.get("/inbox/messages/"+id).then(function(response) {
                that.messageThread = response.data;
                console.log(response.data);
                that.getSender();
            }, function(response) {
                if(response.status == 401) {
                    alert("Nemate ovlašćenja za pristup ovom sadržaju.")
                }
            });
        }

        this.getSender = function() {
            $http.get("/inbox/messages/sender").then(function(response) {
                that.notSender = response.data;
                console.log(response.data);
                for (let i = 0; i < that.messageThread.length; i++) {
                    if (that.notSender != that.messageThread[i].username_from) {
                        that.messageSender = that.messageThread[i].username_from;
                        that.messageReciever = that.messageSender;
                        return;
                    }
                that.messageSender = "You";
                    
                }
            }, function(response) {
                if(response.status == 401) {
                    alert("Nemate ovlašćenja za pristup ovom sadržaju.")
                }
            });
        }

        this.message = {
            "message_from" : null,
            "username": "",
            "content": "",
            "dateCreated": null,
            "message_to" : null

        }

        this.sendMessage = function(){ 
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            var hh = today.getHours();
            var min = today.getMinutes();

            if (dd < 10) {
            dd = '0' + dd;
            }

            if (mm < 10) {
            mm = '0' + mm;
            }

            today = yyyy + '/' + mm + '/' + dd + " " + hh + ":" + min;
            that.message.dateCreated = today;
            that.message.username = that.messageReciever;
            $http.post("/inbox/sendMessage", that.message).then(function(response){
                console.log("Poslata poruka! Kod: " + response.status);
                that.getMessageThread($stateParams["id"]);
            }, function(response) {
                console.log(response.status) 
            });
        };

        this.deleteMessage = function(id) {

            $http.delete("/inbox/message/"+id).then(function(response){
                that.getMessageThread($stateParams["id"]);
            }, function(response){
                console.log("Greska pri uklanjanju robe! Kod: " + response.status);
            });
        }

        this.getMessageThread($stateParams["id"]);
    }]);
})(angular);