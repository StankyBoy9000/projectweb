(function (angular) {

    var app = angular.module("app");

    app.controller("myProfileCtrl", ["$http", "$state", function ($http, $state) {

        var that = this;

        this.likeNumber = 0;
        this.dislikeNumber = 0;

        this.searchParams = {
        }

        this.allAds = [];

        this.userProfile = null;
        this.getUserProfile = function(){
            $http.get("/myProfile/userProfile").then(function(response){
                that.userProfile = response.data;
                that.getLikes();
                that.getDislikes();
            }, function(response) {
                console.log("Error! Code: " + response.status);
            });
        };
        
        this.getUser = function(username) {
            $http.get("/myProfile/"+username).then(function(response) {
                that.userProfile = response.data;
            }, function(response) {
                if(response.status == 401) {
                    alert("Nemate ovlašćenja za pristup ovom sadržaju.")
                }
            });
        }

        this.editUser = function() {
            $http.put("/myProfile/user/"+that.userProfile.username, that.userProfile).then(
                function(response) {
                    console.log(that.userProfile);
                    alert("Nalog uspesno izmenjen.")
                    that.getUserProfile();
                }, function(response) {
                    if(response.status == 401) {
                        alert("Nemate ovlašćenja za pristup ovom sadržaju.")
                    }
                    else if(response.status == 409){
                        alert("Postoji duplikat")
                    }
                }
            )
        }


        this.deleteProfile = function() {
            console.log(that.userProfile.id)
            $http.delete("/myProfile/user/"+that.userProfile.id).then(function(response){
                $state.go('base');
            }, function(response){
                if(response.status == 401) {
                    alert("Nemate ovlašćenja za pristup ovom sadržaju.")
                }
            });
        }

        this.getLikes = function() {
            $http.get("/user/getLikes/"+that.userProfile.username).then(function(response) {
                that.likeNumber = response.data;
            }, function(response) {
                if(response.status == 401) {
                    alert("Nemate ovlašćenja za pristup ovom sadržaju.")
                }
            });
        }

        this.getDislikes = function() {
            $http.get("/user/getDislikes/"+that.userProfile.username).then(function(response) {
                that.dislikeNumber = response.data;
            }, function(response) {
                if(response.status == 401) {
                    alert("Nemate ovlašćenja za pristup ovom sadržaju.")
                }
            });
        }

        this.searchAds = function(x) {
            that.searchParams.sortType = x;
            console.log(that.searchParams)
            $http.get("/ads/profileAd", {params: that.searchParams}).then(function(response){
                that.allAds = response.data;
                console.log(that.allAds)
            }, function(response) {
                console.log("Greska pri dobavljanju robe! Kod: " + response.status);
            })
        }

        this.deleteThisAd = function(id) {
            console.log(id)
            $http.delete("/ads/profileAd/"+id).then(function(response){
                alert("Ad uspesno izbrisan")
                that.searchAds();
            }, function(response){
                if(response.status == 401) {
                    alert("Nemate ovlašćenja za pristup ovom sadržaju.")
                }
            });
        }


        this.searchAds();
        this.getUserProfile();
        

    }]);
})(angular);