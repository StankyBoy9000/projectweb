(function (angular) {

    var app = angular.module("app");

    app.controller("userCtrl", ["$stateParams", "$http", function($stateParams, $http) {

        var that = this;

        this.userSelected = {};

        this.getUserSelected = function(username) {
            $http.get("/user/"+username).then(function(response) {
                that.userSelected = response.data;
                that.searchAds("");
                console.log(that.userSelected.id + "user selected");
            }, function(response) {
                if(response.status == 401) {
                    alert("Nemate ovlašćenja za pristup ovom sadržaju.")
                }
            });
        }

        this.searchParams = {}
        this.allAds = [];

        this.likeObject = {
            "id_from": null,
            "id_to": null
        }

        this.dislikeObject = {
            "id_from": null,
            "id_to": null
        }
        this.likeNumber = 0;
        this.dislikeNumber = 0;
        this.like = function(){ 
            that.likeObject.id_to = that.userSelected.id 
            $http.post("/user/like", that.likeObject).then(function(response){
                console.log("Uspesno kreiran like! Kod: " + response.status);
                that.getLikes($stateParams["username"]);
                that.getDislikes($stateParams["username"]);

            }, function(response) {
                console.log(response.status)    
            });
        };

        this.getLikes = function(username) {
            $http.get("/user/getLikes/"+username).then(function(response) {
                that.likeNumber = response.data;
            }, function(response) {
                if(response.status == 401) {
                    alert("Nemate ovlašćenja za pristup ovom sadržaju.")
                }
            });
        }

        this.dislike = function(){ 
            that.dislikeObject.id_to = that.userSelected.id 
            $http.post("/user/dislike", that.dislikeObject).then(function(response){
                console.log("Uspesno kreiran dislike! Kod: " + response.status);
                that.getDislikes($stateParams["username"]);
                that.getLikes($stateParams["username"]);

            }, function(response) {
                console.log(response.status)    
            });
        };

        this.getDislikes = function(username) {
            $http.get("/user/getDislikes/"+username).then(function(response) {
                that.dislikeNumber = response.data;
            }, function(response) {
                if(response.status == 401) {
                    alert("Nemate ovlašćenja za pristup ovom sadržaju.")
                }
            });
        }

        this.searchAds = function(x) {
            that.searchParams.sortType = x;
            that.searchParams.id = that.userSelected.id;
            console.log(that.searchParams, " user selected")
            $http.get("/ads/showProfileAd", {params: that.searchParams}).then(function(response){
                that.allAds = response.data;
                console.log(that.allAds)
            }, function(response) {
                console.log("Greska pri dobavljanju robe! Kod: " + response.status);
            })
        }

        
        this.getUserSelected($stateParams["username"]);
        this.getLikes($stateParams["username"]);
        this.getDislikes($stateParams["username"]);

    }]);
})(angular);