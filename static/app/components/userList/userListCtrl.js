(function (angular) {
    var app = angular.module("app");

    app.controller("userListCtrl", ["$http", "$state", function ($http, $state) {

        var that = this;

        this.parametriPretrage = {
            username: "",
            phone_number: "",
            email: ""
        }

        this.allUsers = [];
        this.userSelected = null;
        this.isAdmin = false;
 
        this.goToList = function(){
            $state.go("userList", {}, {reload: true});
        }

        this.searchUsers = function() {
            that.parametriPretrage.phone_number = that.parametriPretrage.username
            that.parametriPretrage.email = that.parametriPretrage.username
            $http.get("/user/allUsers",{params: that.parametriPretrage}).then(function(response){
                console.log(response.data + "response data")
                that.allUsers = response.data;

            }, function(response) {
                console.log("Greska pri dobavljanju robe! Kod: " + response.status);
            })
        }

        this.findUserById = function(id){
            console.log("aa")
            for (let i = 0; i < that.allUsers.length; i++) {
                if(that.allUsers[i]["id"] == id){
                    that.userSelected = that.allUsers[i];
                }
                
            }
            console.log(that.userSelected)
        }
        this.editThisUser = function(id) {
            that.findUserById(id);
            console.log(that.userSelected)
            $http.put("/myProfile/"+that.userSelected.username, that.userSelected).then(
                function(response) {
                    console.log(that.userSelected);
                    alert("Nalog uspesno izmenjen.")
                    that.searchUsers();
                }, function(response) {
                    if(response.status == 401) {
                        alert("Nemate ovlašćenja za pristup ovom sadržaju.")
                    }
                    else if(response.status == 400){
                        alert("Neispravan unos")
                    }
                    else if(response.status == 409){
                        alert("Postoji duplikat")
                    }
                }
            )
        }

        this.deleteThisUser = function(id) {
            console.log(id)
            $http.delete("/myProfile/"+id).then(function(response){
                alert("User uspesno izbrisan")
                that.searchUsers();
            }, function(response){
                if(response.status == 401) {
                    alert("Nemate ovlašćenja za pristup ovom sadržaju.")
                }
            });
        }

        this.getIsAdmin = function(){
            $http.get("/user/isAdmin").then(function(response){
                that.isAdmin = response.data;

            }, function(response) {
                console.log("Greska pri dobavljanju robe! Kod: " + response.status);
            })
        }
        this.getIsAdmin();
        this.searchUsers();

    }]);
})(angular);