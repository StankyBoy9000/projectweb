(function (angular) {

    var app = angular.module("app");

    app.controller("signUpCtrl", ["$http", "$state", function ($http, $state) {

        var that = this;
        this.newUser = {
                        "name": "",
                        "surname": "",
                        "username": "",
                        "password": "",
                        "email": "",
                        "city_id": null,
                        "phone_number": "",
                        "gender_id": null,
                        "role": "user"
                    }

        this.countries = [];
        this.country = null;

        this.isCountrySelected = function(){
            if(that.country != null){
                
                return true;
            };
            return false;
        };

        this.getCountries = function(){
            $http.get("/countries").then(function(response){
                if(response.data.length > 0){
                    that.countries = response.data;
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
            });
        };

        this.cities = [];
        this.city = null;
        this.getCities = function(){
            $http.get("/cities/" +that.country).then(function(response){
                if(response.data.length > 0){
                    that.cities = response.data;
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
            });
        };

        this.genders = [];
        this.gender = null;

        this.getGenders = function(){
            $http.get("/genders").then(function(response){
                if(response.data.length > 0){
                    that.genders = response.data;
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
            });
        };

        this.signUp = function(){ 
            console.log(that.newUser)
            $http.post("/user/users", that.newUser).then(function(response){
                console.log("Uspesno kreiran korisnik! Kod: " + response.status);
                alert("Uspesno kreiran nalog!")
                $state.go('base');
            }, function(response) {
                console.log(response.status)
                if(response.status == 409 && response.data == "username duplicate"){
                    window.alert("Username vec postoji")
                }
                else if(response.status == 409 && response.data == "email duplicate"){
                    window.alert("Email vec postoji")
                }
                else{
                    console.log("Greska pri dodavanju korisnika! Kod: " + response.status);
                }
                
            });
        };


        this.onload = function(){
            that.getCountries();
        }

        this.onload();

    }]);
})(angular);