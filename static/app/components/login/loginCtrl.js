(function (angular) {
    var app = angular.module("app");

    app.controller("loginCtrl", ["$http", "$state", function ($http, $state) {

        var that = this;

        this.user = {username: "", password: ""}
        this.state = {loggedIn: false};

        this.login = function() {
            $http.post("/login", that.user).then(function(){
                that.loggedIn = true;
                $state.go("home", {}, {reload: true});
            }, function() {
                alert("Neuspešna prijava!");
            })
        }

        this.logout = function() {
            $http.get("/logout").then(function(){
                that.loggedIn = false;
                $state.go("base", {}, {reload: true});
            }, function() {

            })
        }

    }]);
})(angular);