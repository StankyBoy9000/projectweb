(function (angular) {

    var app = angular.module("app");

    app.controller("adsCtrl", ["$http", "$state","$scope","$window", "$rootScope", function ($http, $state, $window, $rootScope) {

        var that = this;
        this.newPost = {
            "title": "",
            "roomNumber": null,
            "size": null,
            "adress": "",
            "description": "",
            "timeposted": null,
            "users_id": null,
            "sale_rent_id": null,
            "images" : null,
            "img_url" : null
        }
        this.isAdmin = false;
        this.searchParams = {
            "adress": "",
            "minSize" : undefined,
            "maxSize" : undefined,
            "roomNumber" : undefined
        }
        this.allAds = [];
        this.createAd = function(){
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            var hh = today.getHours();
            var min = today.getMinutes();

            if (dd < 10) {
            dd = '0' + dd;
            }

            if (mm < 10) {
            mm = '0' + mm;
            }

            today = yyyy + '/' + mm + '/' + dd + " " + hh + ":" + min;
            that.newPost.timeposted = today;
            
            //that.newPost.img_url = that.newPost.img_url.base64
            console.log(that.newPost)
            $http.post("/ads/ad", that.newPost).then(function(response){
                console.log("Uspesno kreiran post! Kod: " + response.status);
                alert("Uspesno kreiran post!")
                that.searchAds();
            }, function(response) {
                if(response.data == "Neispravan unos"){
                    alert("Neispravan unos")
                }
                console.log(response.status)     
            });
        };

        this.types = [];
        this.type = null;
        this.getTypes = function(){
            $http.get("/ads/types").then(function(response){
                if(response.data.length > 0){
                    that.types = response.data;
                    console.log(that.types)
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
            });
        };

        this.searchAds = function(x) {
            that.searchParams.sortType = x;
            console.log(that.searchParams)
            $http.get("/ads/ad", {params: that.searchParams}).then(function(response){
                that.allAds = response.data;
                console.log(that.allAds)
            }, function(response) {
                console.log("Greska pri dobavljanju robe! Kod: " + response.status);
            })
        }

        this.deleteThisAd = function(id) {
            console.log(id)
            $http.delete("/ads/"+id).then(function(response){
                alert("Ad uspesno izbrisan")
                that.searchAds();
            }, function(response){
                if(response.status == 401) {
                    alert("Nemate ovlašćenja za pristup ovom sadržaju.")
                }
            });
        }

        this.getIsAdmin = function(){
            $http.get("/user/isAdmin").then(function(response){
                that.isAdmin = response.data;

            }, function(response) {
                console.log("Greska pri dobavljanju robe! Kod: " + response.status);
            })
        }

        this.followersObject = {"users_id" : null};

        this.follow = function(id){
            that.followersObject.post_id = id;
            console.log(that.followersObject);
            $http.post("/ads/followers", that.followersObject).then(function(response){
                console.log("Uspesno kreiran follow! Kod: " + response.status);
                that.searchAds();
            }, function(response) {
                if(response.data == "Neispravan unos"){
                    alert("Neispravan unos")
                }
                console.log(response.status)     
            });
        };

        this.unfollow = function(id) {
            console.log(id)
            $http.delete("/ads/followers/"+id).then(function(response){
                console.log("Uspesno obrisan flollow! Kod: ", response.data)
                that.searchAds();
            }, function(response){
                if(response.status == 401) {
                    alert("Nemate ovlašćenja za pristup ovom sadržaju.")
                }
            });
        }



        this.getIsAdmin();

        this.searchAds();
        this.getTypes();

    }]);
})(angular);