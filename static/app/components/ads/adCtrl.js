(function (angular) {

    var app = angular.module("app");

    app.controller("adCtrl", ["$stateParams","$http", "$state","$scope","$window", "$rootScope", function ($stateParams, $http, $state, $window, $rootScope) {

        var that = this;

        this.ad = {};
        this.image_urls = [];

        this.getAd = function(id) {
            $http.get("/ads/ad/"+id).then(function(response) {
                that.ad = response.data;
                console.log(that.ad.id + "id selected");
            }, function(response) {
                if(response.status == 401) {
                    alert("Nemate ovlašćenja za pristup ovom sadržaju.")
                }
            });
        } 
        
        this.getAdPictures = function(id) {
            $http.get("/ads/ad/pictures/"+id).then(function(response) {
                that.image_urls = response.data;
                console.log(that.image_urls)
            }, function(response) {
                if(response.status == 401) {
                    alert("Nemate ovlašćenja za pristup ovom sadržaju.")
                }
            });
        }
        

        this.getAd($stateParams["id"]);
        this.getAdPictures($stateParams["id"]);


    }]);
})(angular);