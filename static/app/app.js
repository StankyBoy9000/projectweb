(function(angular){
    //Kreiranje novog AngularJS modula pod nazivom app.
    //Ovaj modul zavisi od ui.router modula.
    var app = angular.module("app", ["ui.router","naif.base64"]);

    app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
        /*
         * Stanje se definise preko state funkcije iz $stateProvider-a.
         * Funkciji state se prosledjuje objekat stanja koji sadrzi atribute stanja,
         * ili naziv stanja i potom objekat stanja.
         * Pozive state funkcija je moguce ulancavati jer svaka vraca $stateProvider objekat.
         */
        $stateProvider.state({
            name: "home", //Naziv stanja.
            url: "/myProfile", //URL koji se mapira na zadato stanje. Ovaj URL ce zapravo biti vidljiv kao identifikator fragmenta.
            templateUrl: "/app/components/myProfile/myProfile.tpl.html", //URL do sablona za generisanje prikaza.
            controller: "myProfileCtrl", //Kontroler koji je potrebno injektovati u prikaz.
            controllerAs: "mpc" //Posto ce biti upotrebljen imenovani kontroler, navodi se i ime kontrolera.
        }).state({
            name: "sign_up",
            url: "/signUp",
            templateUrl: "app/components/login/signUp.tpl.html",
            controller: "signUpCtrl",
            controllerAs: "suc"
        }).state({
            name: "base",
            url: "/base",
            templateUrl: "app/components/base/base.tpl.html"
        }).state({
            name: "userList",
            url: "/userList",
            templateUrl: "app/components/userList/userList.tpl.html",
            controller: "userListCtrl",
            controllerAs: "ulc"
        }).state({
            name: "user",
            url: "/user/{username: string}",
            templateUrl: "/app/components/userList/user.tpl.html",
            controller: "userCtrl",
            controllerAs: "uc"
        }).state({
            name: "ads",
            url: "/ads",
            templateUrl: "/app/components/ads/ads.tpl.html",
            controller: "adsCtrl",
            controllerAs: "ac"
        }).state({
            name: "inbox",
            url: "/inbox",
            templateUrl: "/app/components/inbox/inbox.tpl.html",
            controller: "inboxCtrl",
            controllerAs: "ic"
        }).state({
            name: "openMsg",
            url: "/openMsg/{id: int}",
            templateUrl: "/app/components/inbox/openMsg.tpl.html",
            controller: "openMsgCtrl",
            controllerAs: "omc"
        })
        .state({
            name: "ad",
            url: "/ad/{id: int}",
            templateUrl: "/app/components/ads/ad.tpl.html",
            controller: "adCtrl",
            controllerAs: "adc"
        });

        //Ukoliko zadata ruta ne odgovara ni jednoj od ruta stanja vrace se na link
        //za home stanje.
        $urlRouterProvider.otherwise("/");
    }]);
})(angular);