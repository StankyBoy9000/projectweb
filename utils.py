import flask

from flaskext.mysql import MySQL

from pymysql.cursors import DictCursor


from functools import wraps


mysql_db = MySQL(cursorclass=DictCursor)

# Dekorator za obradu prava pristupa
def secured(roles=[]):
    def secured_with_roles(f):
        @wraps(f)
        def login_check(*args, **kwargs):
            # Provera ulogovanosti korisnika i prava pristupa
            if flask.session.get("user") is not None and flask.session.get("user")["role"] in roles:
                return f(*args, **kwargs)
            return "", 401
        return login_check
    return secured_with_roles
