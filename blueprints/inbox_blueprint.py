import flask
from flask import request
from flask import Blueprint

from utils import mysql_db, secured

inbox_blueprint = Blueprint("inbox", __name__)

@inbox_blueprint.route("/sendMessage", methods=["POST"])
@secured(roles=["admin", "user"])
def send_message():  
    db = mysql_db.get_db()
    cr = db.cursor()

    requestedObject = dict(request.json)
    print(requestedObject)

    messageObject = {
        "username_from" :"",
        "id_from" : None,
        "id_to" : None,
        "content" : "",
        "dateCreated" : None,
        "username_to" : None

    }
    cr.execute("SELECT * FROM users WHERE username=%s",requestedObject["username"])
    userTo = cr.fetchone()
    messageObject["id_to"] = userTo["id"]
    messageObject["username_to"] = userTo["username"]
    messageObject["id_from"] = flask.session.get("user")["id"]
    messageObject["content"] = requestedObject["content"]
    messageObject["dateCreated"] = requestedObject["dateCreated"]
    messageObject["username_from"] = flask.session.get("user")["username"]
    print(flask.session.get("user")["id"])
    print(messageObject)

    cr.execute("INSERT INTO messages (id_from, id_to, content, dateCreated,username_from,username_to) VALUES(%(id_from)s, %(id_to)s, %(content)s, %(dateCreated)s, %(username_from)s, %(username_to)s)", messageObject)
    db.commit()
    return "", 201

@inbox_blueprint.route("/messages", methods=["GET"])
@secured(roles=["admin", "user"])
def get_messages():
    cr = mysql_db.get_db().cursor()
    tempUser = { "temp_id" : flask.session.get("user")["id"]}
    cr.execute("SELECT * FROM messages WHERE id_to=%(temp_id)s OR id_from=%(temp_id)s ORDER BY dateCreated DESC", tempUser)
    messages = cr.fetchall()
    usernames = []
    filteredMessages = []
    for i in range(len(messages)):
        if messages[i]["username_from"] not in usernames:
            filteredMessages.append(messages[i])
        usernames.append(messages[i]["username_from"])
    return flask.jsonify(filteredMessages)

@inbox_blueprint.route("/messages/<int:msg_id>", methods=["GET"])
@secured(roles=["admin", "user"])
def get_message_thread(msg_id):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM messages WHERE id=%s", (msg_id, ))
    mainMsg = cr.fetchone()
    if mainMsg is not None:
        cr.execute("SELECT * FROM messages WHERE id_from=%(id_from)s AND id_to=%(id_to)s OR id_from=%(id_to)s AND id_to=%(id_from)s ORDER BY dateCreated DESC", mainMsg)
        mainThread = cr.fetchall()
        return flask.jsonify(mainThread)
    mainThread = []
    return flask.jsonify(mainThread)

@inbox_blueprint.route("/messages/sender", methods=["GET"])
@secured(roles=["admin", "user"])
def get_sender():
    data = flask.session.get("user")["username"]
    return flask.jsonify(data)

@inbox_blueprint.route("/message/<int:id_message>", methods=["DELETE"])
@secured(roles=["admin", "user"])
def delete_message(id_message):
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("DELETE FROM messages WHERE id=%s", (id_message,))
    db.commit()
    return "", 204 # https://www.ietf.org/rfc/rfc2616.txt