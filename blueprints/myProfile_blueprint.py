import flask
from flask import request
from flask import Blueprint

from utils import mysql_db, secured

myProfile_blueprint = Blueprint("myProfile_blueprint", __name__)

@myProfile_blueprint.route("/userProfile", methods=["GET"])
@secured(roles=["admin", "user"])
def get_userProfile():
    if flask.session.get("user") is not None:
        username = flask.session.get("user")["username"]
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM users WHERE username=%s", (username, ))
    userProfile = cr.fetchone()
    print(userProfile)
    return flask.jsonify(userProfile)

@myProfile_blueprint.route("/<string:username>", methods=["GET"])
@secured(roles=["admin", "user"])
def get_user(username):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM korisnici WHERE username=%s", (username, ))
    user = cr.fetchone()
    return flask.jsonify(user)

@myProfile_blueprint.route("/user/<string:username>", methods=["PUT"])
@secured(roles=["admin", "user"])
def edit_user_user(username):
    db = mysql_db.get_db()
    cr = db.cursor()
    data = dict(request.json)
    print(data)
    data["username"] = username
    try:
        cr.execute("UPDATE users SET username=%(username)s, name=%(name)s, surname=%(surname)s, email=%(email)s, phone_number=%(phone_number)s WHERE id=%(id)s", data)
        db.commit()
        return "", 200
    except KeyError:
        return "Neispravan unos",400
    #except:
        #return "Duplikat",409 #Znam da ovo hvata vise od samo duplikata, ali nisam znao kako da uhvatim konkretno pymysql.IntegrityError.
    


@myProfile_blueprint.route("/user/<int:id>", methods=["DELETE"])
@secured(roles=["admin", "user"])
def delete_user_user(id):
    print("id",id)
    db = mysql_db.get_db()
    cr = db.cursor()
    objectId = {
        "id":None
    }
    objectId["id"] = id
    cr.execute("DELETE FROM messages WHERE id_from=%(id)s OR id_to=%(id)s", (objectId))
    cr.execute("DELETE FROM likes WHERE like_from_id=%(id)s OR like_to_id=%(id)s", (objectId))
    cr.execute("DELETE FROM dislikes WHERE dislike_from_id=%(id)s OR dislike_to_id=%(id)s", (objectId))
    cr.execute("DELETE FROM followers WHERE users_id=%(id)s ", (objectId))

    cr.execute("SELECT * FROM post WHERE users_id=%(id)s", (objectId))
    users_posts_id = cr.fetchall()
    for i in users_posts_id:
        cr.execute("DELETE FROM image WHERE post_id=%(id)s", (i))
    cr.execute("DELETE FROM post WHERE users_id=%(id)s ", (objectId))

    cr.execute("DELETE FROM users WHERE id=%(id)s", (objectId))
    db.commit()
    return "", 204

@myProfile_blueprint.route("/<string:username>", methods=["PUT"])
@secured(roles=["admin"])
def edit_user(username):
    db = mysql_db.get_db()
    cr = db.cursor()
    data = dict(request.json)
    print(data)
    data["username"] = username
    try:
        cr.execute("UPDATE users SET username=%(username)s, name=%(name)s, surname=%(surname)s, email=%(email)s, phone_number=%(phone_number)s WHERE id=%(id)s", data)
        db.commit()
        return "", 200
    except KeyError:
        return "Neispravan unos",400
    #except:
        #return "Duplikat",409 #Znam da ovo hvata vise od samo duplikata, ali nisam znao kako da uhvatim konkretno pymysql.IntegrityError.
    


@myProfile_blueprint.route("/<int:id>", methods=["DELETE"])
@secured(roles=["admin"])
def delete_user(id):
    print("id",id)
    db = mysql_db.get_db()
    cr = db.cursor()
    objectId = {
        "id":None
    }
    objectId["id"] = id
    cr.execute("DELETE FROM messages WHERE id_from=%(id)s OR id_to=%(id)s", (objectId))
    cr.execute("DELETE FROM likes WHERE like_from_id=%(id)s OR like_to_id=%(id)s", (objectId))
    cr.execute("DELETE FROM dislikes WHERE dislike_from_id=%(id)s OR dislike_to_id=%(id)s", (objectId))
    cr.execute("DELETE FROM followers WHERE users_id=%(id)s ", (objectId))

    cr.execute("SELECT * FROM post WHERE users_id=%(id)s", (objectId))
    users_posts_id = cr.fetchall()
    for i in users_posts_id:
        cr.execute("DELETE FROM image WHERE post_id=%(id)s", (i))
    cr.execute("DELETE FROM post WHERE users_id=%(id)s ", (objectId))

    cr.execute("DELETE FROM users WHERE id=%(id)s", (objectId))
    db.commit()
    return "", 204

