import flask
from flask import request
from flask import Blueprint

from utils import mysql_db, secured

user_blueprint = Blueprint("user", __name__)
# Rad sa korisnicima

@user_blueprint.route("/allUsers", methods=["GET"])
@secured(roles=["admin", "user"])
def get_Searched_users():
    cr = mysql_db.get_db().cursor()
    createdObject = {
        "username" : (request.args.get("username")),
        "phone_number": (request.args.get("phone_number")),
        "email" : (request.args.get("email"))
    }
    if (request.args.get("username")) is not "":
        arg = (request.args.get("input"), )
        cr.execute("SELECT * FROM users WHERE username=%(username)s OR phone_number=%(phone_number)s OR email=%(email)s",createdObject)
    else:
        cr.execute("SELECT * FROM users")
    stavke = cr.fetchall()
    return flask.json.jsonify(stavke)

@user_blueprint.route("/users", methods=["POST"])
def add_user():
    
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("SELECT * FROM users")
    userData = cr.fetchall()
    for i in userData:
        if i["username"] == dict(request.json)["username"] :
            return "username duplicate", 409
        elif i["email"] == dict(request.json)["email"]:
            return "email duplicate", 409
    cr.execute("INSERT INTO users (username, password, name, surname, email, phone_number, city_id, gender_id, role) VALUES(%(username)s, %(password)s, %(name)s, %(surname)s, %(email)s, %(phone_number)s, %(city_id)s, %(gender_id)s, %(role)s)", dict(request.json))
    db.commit()
    return "", 201



@user_blueprint.route("/<string:username>", methods=["PUT"])
@secured(roles=["admin", "user"])
def edit_user(username):
    db = mysql_db.get_db()
    cr = db.cursor()
    data = dict(request.json)
    data["username"] = username
    cr.execute("UPDATE users SET name=%(name)s, username=%(username)s, surname=%(surname)s, email=%(email)s, phone_number=%(phone_number)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200

@user_blueprint.route("/<string:username>", methods=["GET"])
@secured(roles=["admin", "user"])
def get_user(username):
    print("usao u usera")
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM users WHERE username=%s", (username, ))
    user = cr.fetchone()
    return flask.jsonify(user)

@user_blueprint.route("/like", methods=["POST"])
@secured(roles=["admin", "user"])
def add_like():
    
    db = mysql_db.get_db()
    cr = db.cursor()
    likeObject = dict(request.json)

    likeObject["id_from"] = flask.session.get("user")["id"]
    print(flask.session.get("user"))

    cr.execute("SELECT * FROM likes")
    userData = cr.fetchall()

    cr.execute("SELECT * FROM dislikes")
    dislikeData = cr.fetchall()

    for i in dislikeData:
        if i["dislike_from_id"] == likeObject["id_from"] and i["dislike_to_id"] == likeObject["id_to"]:
            deleteDislike(likeObject)
    for i in userData:
        if i["like_from_id"] == likeObject["id_from"] and i["like_to_id"] == likeObject["id_to"]:
            deleteLike(likeObject)
            return "Like deleted", 204
    cr.execute("INSERT INTO likes (like_from_id, like_to_id) VALUES(%(id_from)s, %(id_to)s)", likeObject)
    db.commit()
    return "", 201

@user_blueprint.route("/dislike", methods=["POST"])
@secured(roles=["admin", "user"])
def add_dislike():
    
    db = mysql_db.get_db()
    cr = db.cursor()
    dislikeObject = dict(request.json)

    dislikeObject["id_from"] = flask.session.get("user")["id"]

    cr.execute("SELECT * FROM dislikes")
    userData = cr.fetchall()

    cr.execute("SELECT * FROM likes")
    likeData = cr.fetchall()

    for i in likeData:
        if i["like_from_id"] == dislikeObject["id_from"] and i["like_to_id"] == dislikeObject["id_to"]:
            deleteLike(dislikeObject)

    for i in userData:
        if i["dislike_from_id"] == dislikeObject["id_from"] and i["dislike_to_id"] == dislikeObject["id_to"]:
            deleteDislike(dislikeObject)
            return "Disike deleted", 204
    cr.execute("INSERT INTO dislikes (dislike_from_id, dislike_to_id) VALUES(%(id_from)s, %(id_to)s)", dislikeObject)
    db.commit()
    return "", 201

@user_blueprint.route("/getLikes/<string:username>", methods=["GET"])
@secured(roles=["admin", "user"])
def get_likes(username):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM users WHERE username=%s", (username, ))
    user = cr.fetchone()

    cr.execute("SELECT * FROM likes WHERE like_to_id=%s", (user["id"]), )
    likes = cr.fetchall()
    like_counter = len(likes)
    return flask.jsonify(like_counter)

@user_blueprint.route("/getDislikes/<string:username>", methods=["GET"])
@secured(roles=["admin", "user"])
def get_dislikes(username):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM users WHERE username=%s", (username, ))
    user = cr.fetchone()

    cr.execute("SELECT * FROM dislikes WHERE dislike_to_id=%s", (user["id"]), )
    dislikes = cr.fetchall()
    dislike_counter = len(dislikes)
    return flask.jsonify(dislike_counter)

def deleteLike(likeObject):
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("DELETE FROM likes WHERE like_from_id=%(id_from)s AND like_to_id=%(id_to)s", likeObject)
    db.commit()
    return "", 204

def deleteDislike(dislikeObject):
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("DELETE FROM dislikes WHERE dislike_from_id=%(id_from)s AND dislike_to_id=%(id_to)s", dislikeObject)
    db.commit()
    return "", 204


@user_blueprint.route("/isAdmin", methods=["GET"])
@secured(roles=["admin", "user"])
def get_is_admin():
    return flask.jsonify(flask.session.get("user")["role"] == "admin")