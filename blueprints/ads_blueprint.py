import flask
import datetime
from utils import mysql_db, secured

from flask import Blueprint
from flask import request
from functools import wraps

ads_blueprint = Blueprint("ads", __name__)

@ads_blueprint.route("/types", methods=["GET"])
@secured(roles=["admin", "user"])
def get_all_types():
    query = "SELECT * FROM sale_rent"
    cr = mysql_db.get_db().cursor()
    cr.execute(query,)
    types = cr.fetchall()
    return flask.json.jsonify(types)

@ads_blueprint.route("/ad", methods=["POST"])
@secured(roles=["admin", "user"])
def add_post():
    img_url = {"img": None,
               "post_id" : None}
    db = mysql_db.get_db()
    cr = db.cursor()
    post_object = dict(request.json)
    images = post_object["images"]
    post_object["images"] = None
    post_object["users_id"] = flask.session.get("user")["id"]
    post_object["user_username"] = flask.session.get("user")["username"] 
    post_object["img_url"] = images[0]["base64"]
    try:
        cr.execute("INSERT INTO post (user_username, main_img_url, price, title, roomNumber, size, adress, description, timeposted, users_id, sale_rent_id) VALUES(%(user_username)s, %(img_url)s, %(price)s, %(title)s, %(roomNumber)s, %(size)s, %(adress)s, %(description)s, %(timeposted)s, %(users_id)s, %(sale_rent_id)s)", post_object)
        
        cr.execute("SELECT * FROM post")
        posts = cr.fetchall()
        currentPost_id = posts[0]["id"]
        for i in posts:
            if i["id"] > currentPost_id:
                currentPost_id = i["id"]
        img_url["post_id"] = currentPost_id
        for i in images:
            img_url["img"] = i["base64"]
            print(img_url["post_id"])
            cr.execute("INSERT INTO image (img_code, post_id) VALUES( %(img)s, %(post_id)s )", img_url)
        db.commit()

    except KeyError:
        return "Neispravan unos",400
    except TypeError:
        return "Neispravan unos",400
    return "", 201

@ads_blueprint.route("/ad", methods=["GET"])
@secured(roles=["admin", "user"])
def getSearchedAds():
    cr = mysql_db.get_db().cursor()
    upit = "SELECT * FROM post"
    selekcija = " WHERE "
    parametri_pretrage = []
    sort_type = request.args.get("sortType")

    if request.args.get("adress") is not None:
        parametri_pretrage.append("%" + request.args.get("adress") + "%")
        selekcija += "adress LIKE %s "

    try:
        parametri_pretrage.append(int(request.args.get("minSize")))
        if len(parametri_pretrage) > 1:
            selekcija += "AND "
        selekcija += "size >= %s "
    except:
        pass

    try:
        parametri_pretrage.append(int(request.args.get("maxSize")))
        if len(parametri_pretrage) > 1:
            selekcija += "AND "
        selekcija += "size <= %s "
    except:
        pass

    try:
        parametri_pretrage.append(int(request.args.get("minPrice")))
        if len(parametri_pretrage) > 1:
            selekcija += "AND "
        selekcija += "price >= %s "
    except:
        pass

    try:
        parametri_pretrage.append(int(request.args.get("maxPrice")))
        if len(parametri_pretrage) > 1:
            selekcija += "AND "
        selekcija += "price <= %s "
    except:
        pass

    try:
        parametri_pretrage.append(int(request.args.get("roomNumber")))
        if len(parametri_pretrage) > 1:
            selekcija += "AND "
        selekcija += "roomNumber = %s "
    except:
        pass

    if sort_type == "rooms":
        selekcija += "ORDER BY roomNumber DESC"
    elif sort_type == "size":
        selekcija += "ORDER BY size DESC"
    elif sort_type == "title" or "":
        selekcija += "ORDER BY title DESC"
    elif sort_type == "date":
        selekcija += "ORDER BY timeposted DESC"
    elif sort_type == "adress":
        selekcija += "ORDER BY adress DESC"
    elif sort_type == "user":
        selekcija += "ORDER BY user_username DESC"
    elif sort_type == "price":
        selekcija += "ORDER BY price DESC"

    
    if len(parametri_pretrage) > 0:
        upit += selekcija

    
    cr.execute(upit, parametri_pretrage)
    stavke = cr.fetchall()
   

    return flask.json.jsonify(stavke)

@ads_blueprint.route("/profileAd", methods=["GET"])
@secured(roles=["admin", "user"])
def get_profile_ads():
    cr = mysql_db.get_db().cursor()
    selekcija = "SELECT * FROM post WHERE users_id=%s "
    sort_type = request.args.get("sortType")


    if sort_type == "rooms":
        selekcija += "ORDER BY roomNumber DESC"
    elif sort_type == "size":
        selekcija += "ORDER BY size DESC"
    elif sort_type == "title" or "":
        selekcija += "ORDER BY title DESC"
    elif sort_type == "date":
        selekcija += "ORDER BY timeposted DESC"
    elif sort_type == "adress":
        selekcija += "ORDER BY adress DESC"
    elif sort_type == "user":
        selekcija += "ORDER BY user_username DESC"
    elif sort_type == "price":
        selekcija += "ORDER BY price DESC"

    cr.execute(selekcija, flask.session.get("user")["id"])
    stavke = cr.fetchall()
   

    return flask.json.jsonify(stavke)

@ads_blueprint.route("/showProfileAd", methods=["GET"])
@secured(roles=["admin", "user"])
def get_show_profile_ads():
    print("USAOOOO\n")
    cr = mysql_db.get_db().cursor()
    selekcija = "SELECT * FROM post WHERE users_id=%s "
    sort_type = request.args.get("sortType")


    if sort_type == "rooms":
        selekcija += "ORDER BY roomNumber DESC"
    elif sort_type == "size":
        selekcija += "ORDER BY size DESC"
    elif sort_type == "title" or "":
        selekcija += "ORDER BY title DESC"
    elif sort_type == "date":
        selekcija += "ORDER BY timeposted DESC"
    elif sort_type == "adress":
        selekcija += "ORDER BY adress DESC"
    elif sort_type == "user":
        selekcija += "ORDER BY user_username DESC"
    elif sort_type == "price":
        selekcija += "ORDER BY price DESC"

    print(request.args.get("id"))
    cr.execute(selekcija, request.args.get("id"))
    stavke = cr.fetchall()
   

    return flask.json.jsonify(stavke)

@ads_blueprint.route("/<int:id>", methods=["DELETE"])
@secured(roles=["admin"])
def delete_ad(id):
    db = mysql_db.get_db()
    cr = db.cursor()
    objectId = {
        "id":None
    }
    objectId["id"] = id
    cr.execute("SELECT * FROM post WHERE users_id=%(id)s", (objectId))
    users_posts_id = cr.fetchall()
    cr.execute("DELETE FROM image WHERE post_id=%(id)s ", (objectId))
    cr.execute("DELETE FROM followers WHERE post_id=%(id)s ", (objectId))
    cr.execute("DELETE FROM post WHERE id=%(id)s", (objectId))
    db.commit()
    return "", 204

@ads_blueprint.route("/profileAd/<int:id>", methods=["DELETE"])
@secured(roles=["admin", "user"])
def delete_profile_ad(id):
    db = mysql_db.get_db()
    cr = db.cursor()
    objectId = {
        "id":None
    }
    objectId["id"] = id
    cr.execute("DELETE FROM followers WHERE post_id=%(id)s ", (objectId))
    cr.execute("DELETE FROM image WHERE post_id=%(id)s ", (objectId))
    cr.execute("DELETE FROM post WHERE id=%(id)s", (objectId))
    db.commit()
    return "", 204

@ads_blueprint.route("/ad/<int:id>", methods=["GET"])
@secured(roles=["admin", "user"])
def get_single_ad(id):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM post WHERE id=%s", (id, ))
    mainAd = cr.fetchone()
    return flask.jsonify(mainAd)

@ads_blueprint.route("/ad/pictures/<int:id>", methods=["GET"])
@secured(roles=["admin", "user"])
def get_single_ad_pictures(id):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM image WHERE post_id=%s", (id, ))
    images = cr.fetchall()
    return flask.jsonify(images)

@ads_blueprint.route("/followers", methods=["POST"])
@secured(roles=["admin", "user"])
def add_follower():
    
    db = mysql_db.get_db()
    cr = db.cursor()
    post_object = dict(request.json)
    post_object["users_id"] = flask.session.get("user")["id"]
    cr.execute("SELECT * FROM followers WHERE users_id=%(users_id)s AND post_id=%(post_id)s", post_object)
    follows = cr.fetchall()
    if follows == ():
        try:
            cr.execute("INSERT INTO followers (users_id, post_id) VALUES(%(users_id)s, %(post_id)s)", post_object)
            db.commit()
            return "", 201
        except KeyError:
            return "Neispravan unos",400
    return "duplikat",409 
    
    

@ads_blueprint.route("followers/<int:id>", methods=["DELETE"])
@secured(roles=["admin", "user"])
def delete_follower(id):
    db = mysql_db.get_db()
    cr = db.cursor()
    objectId = {
        "post_id":None,
        "users_id": flask.session.get("user")["id"]
    }
    objectId["post_id"] = id
    cr.execute("DELETE FROM followers WHERE post_id=%(post_id)s AND users_id=%(users_id)s", (objectId))
    db.commit()
    return "", 204
