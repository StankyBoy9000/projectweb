import flask
import datetime
import main

from flask import Blueprint
from flask import request
from functools import wraps
from utils import mysql_db, secured


countries_cities_blueprint = Blueprint("countries_cities_blueprint", __name__)

# GET ALL COUNTRIES
@countries_cities_blueprint.route("/countries", methods=["GET"])
def get_all_countries():
    query = "SELECT * FROM country"
    cr = main.mysql_db.get_db().cursor()
    cr.execute(query,)
    countries = cr.fetchall()
    return flask.json.jsonify(countries)

@countries_cities_blueprint.route("/cities/<int:country_id>", methods=["GET"])
def get_all_cities(country_id):
    query = "SELECT * FROM city WHERE country_id LIKE %s"
    cr = main.mysql_db.get_db().cursor()
    cr.execute(query, (country_id,))
    cities = cr.fetchall()
    return flask.json.jsonify(cities)

@countries_cities_blueprint.route("/genders", methods=["GET"])
def get_all_genders():
    query = "SELECT * FROM gender"
    cr = main.mysql_db.get_db().cursor()
    cr.execute(query,)
    genders = cr.fetchall()
    return flask.json.jsonify(genders)