import flask
import datetime
from flask import Flask
from flask import request

from utils import mysql_db, secured  

from blueprints.user_blueprint import user_blueprint
from blueprints.countries_cities_blueprint import countries_cities_blueprint
from blueprints.myProfile_blueprint import myProfile_blueprint
from blueprints.ads_blueprint import ads_blueprint
from blueprints.inbox_blueprint import inbox_blueprint

app = Flask(__name__, static_url_path="")
app.secret_key = "sta god"

# Registrovanje blueprinta za rad sa korisnicima.
app.register_blueprint(user_blueprint, url_prefix="/user")
app.register_blueprint(countries_cities_blueprint)
app.register_blueprint(myProfile_blueprint,url_prefix="/myProfile")
app.register_blueprint(ads_blueprint,url_prefix="/ads")
app.register_blueprint(inbox_blueprint,url_prefix="/inbox")
# Konfiguracija za povezivanje na bazu podataka.
app.config['MYSQL_DATABASE_USER'] = 'root' # Korisnicko ime korisnika baze podataka.
app.config['MYSQL_DATABASE_PASSWORD'] = 'root' # Lozinka izabranog korisnika. 
app.config['MYSQL_DATABASE_DB'] = 'mydb' # Ime seme baze podataka koja se koristi.

# Preuzimanje podataka iz konfiguracije i konfigurisanje
# instance MySQL klase.
mysql_db.init_app(app)

@app.route("/")
@app.route("/index")
def index():
    return app.send_static_file("index.html") # Dostavlja datoteku iz static direktorijuma.
    

@app.route("/login", methods=["POST"])
def login():
    cr = mysql_db.get_db().cursor()
    # Pokusaj da se pronadje korisnik sa zadatim korisnickim imenom i lozinkom.
    cr.execute("SELECT * FROM users WHERE username=%(username)s AND password=%(password)s", flask.request.json)
    user = cr.fetchone()
    if user is not None:
        # Ukoliko korisnik postoji zapisuje se u sesiju.
        flask.session["user"] = user
        #print(flask.session.get("user")["username"])
        return "", 200

    return "", 401

@app.route("/logout")
def logout():
    # Prilikom odjave korisnik se brise iz sesije.
    flask.session.pop("user", None)
    return "", 200


if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True)

# Nakon pokretanja aplikacije u web browser je moguce
# uneti URL http://localhost:5000 i videti rezultat ove skripte.